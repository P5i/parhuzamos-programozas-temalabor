﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Producer_Consumer_NET {
    public class DeactivatableConcurrentQueue<T> where T : class {

        private LinkedList<T> linkedList = new LinkedList<T>();
        private readonly object listSync = new object();
        
        public bool Activated { get; private set; } = true;

        private T pendingObj = null; 

        // null-t ad vissza, ha nincs engedélyezve (=> használva van a szín már)
        public T Dequeue() {
            T result;

            // activated helyett signal? -> nem mert jelezni akarom h ezt ne válassza, próbáljon másikat

            lock (listSync) {
                if (!Activated || linkedList.Count == 0) {
                    return null;
                }

                result = linkedList.Last.Value;
                pendingObj = result;
                linkedList.RemoveLast();

                Activated = false;
            }
            return result;
        }

        public T Peek() {
            T result;
            lock (listSync) {
                if (linkedList.Count == 0) {
                    return null;
                }

                result = linkedList.Last.Value;
            }
            return result;
        }

        public void Enqueue(T newItem) {
            lock (listSync) {
                linkedList.AddFirst(newItem);
            }
        }
        
        // így jeleznek ha már újra lehet használni
        public void Activate() {
            lock (listSync) {
                Activated = true;
            }
        }

        public void PutBack() {
            lock (listSync) {
                linkedList.AddLast(pendingObj);
                pendingObj = null;
                Activated = true;
            }
        }

        public int Size() {
            return linkedList.Count;
        }
    }
}
