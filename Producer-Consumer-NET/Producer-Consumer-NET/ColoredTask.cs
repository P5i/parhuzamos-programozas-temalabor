﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Producer_Consumer_NET {

    public class ColoredTask {
        public Color Color { get; private set; }
        public Action Function { get; set; }
        public int Age { get; private set; } = 0;
        public int ID { get; private set; }
        public bool Finished { get; private set; } = false;

        static private int InstanceCnt = 0;

        public ColoredTask(Color color, Action func) {
            this.Color = color;
            this.Function = func;
            ID = InstanceCnt++;
        }

        public ColoredTask(Color color) {
            this.Color = color;
            ID = InstanceCnt++;
        }

        public void IncreaseAge() {
            Age++;
        }

        public void SetFinished() {
            Finished = true;
        }
    }
}
