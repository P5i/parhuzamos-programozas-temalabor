﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Producer_Consumer_NET {
    public class Scheduler {

        private DeactivatableConcurrentQueue<ColoredTask>[] queues;
        private List<ColoredTask> tasks = new List<ColoredTask>();

        private readonly object syncObj = new object();

        public Scheduler(int queueCnt) {
            queues = new DeactivatableConcurrentQueue<ColoredTask>[queueCnt];
            for (int i = 0; i < queueCnt; i++) {
                queues[i] = new DeactivatableConcurrentQueue<ColoredTask>();
            }
        }

        public void AddTask(ColoredTask newTask) {

            lock (syncObj) {
                foreach (var task in tasks) {
                    task.IncreaseAge();
                }
                tasks.Add(newTask);
                Console.WriteLine($"Added: {newTask.ID} {newTask.Color}");

                queues[(int)newTask.Color].Enqueue(newTask);

                Monitor.Pulse(syncObj);
            }                      
        }

        public ColoredTask GetTask() {

            Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} :: Called: GetTask()");

            ColoredTask result = null;

            lock (syncObj) {

                while (result == null) {

                    var validQueues = queues
                         .Where(q => q.Activated)
                         .Where(q => q.Peek() != null)
                         .ToList();

                    if (validQueues.Count != 0) {
                        DeactivatableConcurrentQueue<ColoredTask> queue = validQueues[0];
                        for (int i = 1; i < validQueues.Count; i++) {
                            if (queue.Peek().Age < validQueues[i].Peek().Age) {
                                queue = validQueues[i];
                            }
                        }
                        result = queue.Dequeue();
                    }

                    if (result != null) {
                        Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} :: Got: {result.ID} {result.Color}");
                        return result;
                    }

                    Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} :: Waiting...");

                    // It releases the lock(s) during Wait, so other threads can acquire it. It reclaims the lock(s) when it wakes up.
                    // Azért kell, hogy ha üres akkor elaltassa a szálat
                    Monitor.Wait(syncObj);

                    Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} :: After Monitor.Wait");
                }
            }
            return null;
        }

        public void OnFailure(Color color) {
            queues[(int)color].PutBack();
        }

        public void FinishTaskSignal(Color color) {
            queues[(int)color].Activate();
        }

    }
}
