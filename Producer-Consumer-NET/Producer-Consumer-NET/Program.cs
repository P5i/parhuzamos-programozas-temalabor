﻿
#if false
#define TEST_TO
#endif

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Producer_Consumer_NET {

    // Azért van itt, mert ettől függ a program működése
    public enum Color {
        RED = 0,
        GREEN,
        BLUE,
        PEACH, 
        CYCLAMEN,
        SIZE
    }

    public class Program {

        private static List<ColoredTask> testTasks = new List<ColoredTask>() {
            new ColoredTask(Color.BLUE, () => { FibonacciTask(35); }),
            new ColoredTask(Color.BLUE, () => { FibonacciTask(35); }),
            new ColoredTask(Color.GREEN, () => { FibonacciTask(37); }),
            new ColoredTask(Color.RED, () => { FibonacciTask(35); }),
            new ColoredTask(Color.BLUE, () => { FibonacciTask(35); }),
        };

        private static List<Tuple<int, ColoredTask>> executionOrder = new List<Tuple<int, ColoredTask>>();
        private static readonly object executionOrderLock = new object();     

        static int Main(string[] args) {

#if TEST_TO
            if (args.Length != 3) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter a consumerThreadCnt, colorCnt and runCnt!");
                return 1;
            }
#else 
            if (args.Length != 1) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter a consumer thread count!");
                return 1;
            }
#endif

            if (!int.TryParse(args[0], out int consumerThreadCnt)) // 1-12
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter an integer for the consumerThreadCnt!");
                return 1;
            }
#if TEST_TO
            if (consumerThreadCnt > 12 || consumerThreadCnt < 1) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter the right size of consumerThreadCnt");
                return 1;
            }

            if (!int.TryParse(args[1], out int colorCnt)) // 1-5
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter an integer for the colorCnt!");
                return 1;
            }
            if (colorCnt > 5 || colorCnt < 1) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter the right size of colorCnt");
                return 1;
            }

            if (!int.TryParse(args[2], out int runCnt)) // 1-5
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter an integer for the colorCnt!");
                return 1;
            }
            if (runCnt == 11) {
                return 1;
            }
#else 
            int colorCnt = (int)Color.SIZE;
#endif

            var scheduler = new Scheduler(colorCnt);

#if TEST_TO
            var throughOutputProducer = new Thread(() => ThroughOutputProducer(scheduler, colorCnt));
            throughOutputProducer.Start();

            var consumerThreads = new List<Thread>(consumerThreadCnt);
            for (int i = 0; i < consumerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Consumer(scheduler)));
                consumerThreads[i].IsBackground = true;
                consumerThreads[i].Start();
            }

            var timeObserver = new Thread(() => TimerObserver(colorCnt, consumerThreadCnt, runCnt));
            timeObserver.Start();
#else
            /*var producerTestThread = new Thread(() => TestProducer(scheduler));
             producerTestThread.Start();*/

            var producerThread = new Thread(() => Producer(scheduler));
            producerThread.Start();

            Thread.Sleep(10);

            var consumerThreads = new List<Thread>(consumerThreadCnt);
            for (int i = 0; i < consumerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Consumer(scheduler)));
                consumerThreads[i].Start();
            }                
#endif
            return 0;
        }         

        private static readonly int[] ProducerSleepTimes = Enumerable
                            .Range(1, 10)
                            .Select(i => i * 100)
                            .ToArray();

        private static void Producer(Scheduler scheduler) {

            while (true) {
                var random = new Random();
                var task = new ColoredTask(RandomColor(random, Enum.GetValues(typeof(Color)).Length - 1), () => { FibonacciTask(random.Next(30, 40)); } );
                scheduler.AddTask(task);

                Thread.Sleep(ProducerSleepTimes[random.Next(10)]);
            }            
        }

        private static void TestProducer(Scheduler scheduler) {
            var random = new Random();
            foreach (var task in testTasks) {
                scheduler.AddTask(task);
                Thread.Sleep(1);
            }
        }

        private static void ThroughOutputProducer(Scheduler scheduler, int colorCnt) {
            while (true) {
                var random = new Random();
                var task = new ColoredTask(RandomColor(random, colorCnt), () => { FibonacciTask(35); });
                scheduler.AddTask(task);
                
                Thread.Sleep(1);
            }
        }

        private static void TimerObserver(int colorCnt, int threadCnt, int runCnt, string results = "result.txt") {

            Thread.Sleep(20000);
            Console.WriteLine("Time has come!");

            int cnt = 0;
            lock (executionOrderLock) {
                cnt = executionOrder.Count;
            }

            string path = AppDomain.CurrentDomain.BaseDirectory + results;

            string line = $"{colorCnt};{threadCnt};{cnt}";

            if (!File.Exists(path)) {
                var myFile = File.Create(path);
                myFile.Close();
                TextWriter tw = new StreamWriter(path);
                tw.WriteLine("ColorCnt;ThreadCnt;ThroughOutput");
                tw.WriteLine(line);
                tw.Close();
            }
            else if (File.Exists(path)) {
                TextWriter tw = new StreamWriter(path, append: true);
                tw.WriteLine(line);
                tw.Close();
            }
            threadCnt++;

            if (colorCnt == 5 && threadCnt == 13) {
                runCnt++;
                colorCnt = 1;
                threadCnt = 1;
            }

            if (threadCnt == 13) {
                threadCnt = 1;
                colorCnt++;
            }            

            var startInfo = new ProcessStartInfo {
                FileName = "Producer-Consumer-NET.exe",
                Arguments = $"{threadCnt} {colorCnt} {runCnt}"
            };
            Process.Start(startInfo);

            Environment.Exit(Environment.ExitCode);
        }


        public static void Consumer(Scheduler scheduler) {
            ColoredTask task = null;

            while (true) {
                try {
                    task = scheduler.GetTask();
                    task.Function();
                }
                catch (ThreadAbortException ex) {
                    Thread.ResetAbort(); // Ne hívja újra a ThreadAbortException-t
                    scheduler.OnFailure(task.Color);
                }
                scheduler.FinishTaskSignal(task.Color);
                Console.WriteLine($"F {Thread.CurrentThread.ManagedThreadId} :: {task.ID} {task.Color}");
                lock (executionOrderLock) {
                    executionOrder.Add(new Tuple<int, ColoredTask>(Thread.CurrentThread.ManagedThreadId, task));
                }
                task.SetFinished();
            }
        }

        public static Color RandomColor(Random random, int maxValue) {
            return (Color)Enum.GetValues(typeof(Color)).GetValue(random.Next(maxValue));
        }

        public static void FibonacciTask(int n = 40) {
            if (Fibonacci(n) < 0) {
                Console.WriteLine("Not gonna happen!");
            }
        }

        private static int Fibonacci(int n) {
            if (n < 2) {
                return n;
            }
            return Fibonacci(n - 2) + Fibonacci(n - 1);
        }
    }
}
