# 5. feladat: feladat ütemezésre szolgáló adatstruktúra

A méréseket egy Intel Core i7-6700HQ CPU-n végeztem.   
- alapsebessége: 2.6 GHz, de a méréseken elérte a 3.3 GHz is, az átlaga ~3.15 GHz volt  
- magok száma: 4  
- logikai magok száma: 8  
  

## Technical report

A dokumentációt a következő [linken](./TechnicalReport/technical_report_final.pdf) lehet elérni.