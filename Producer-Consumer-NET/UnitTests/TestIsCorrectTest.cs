﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Producer_Consumer_NET;
using static Producer_Consumer_NET.Tests.SchedulerTests;
using Producer_Consumer_NET.Tests;

namespace UnitTests {
    [TestClass]
    public class TestIsCorrectTest {

        [TestMethod()]
        public void TestRule1() {

            var ct1 = new ColoredTask(Color.BLUE, () => { });
            var ct2 = new ColoredTask(Color.RED, () => { });


            var eventList = new EventList();
            eventList.Add(new Event(EventType.STARTED, ct1));
            eventList.Add(new Event(EventType.STARTED, ct2));
            eventList.Add(new Event(EventType.STARTED, ct1));


            Assert.IsFalse(SchedulerTests.IsCorrectOrder(eventList));
        }

        [TestMethod()]
        public void TestRule3() {

            var ct1 = new ColoredTask(Color.BLUE, () => { });
            var ct2 = new ColoredTask(Color.RED, () => { });
            var ct3 = new ColoredTask(Color.BLUE, () => { });                     


            var eventList = new EventList();
            eventList.Add(new Event(EventType.STARTED, ct1));
            eventList.Add(new Event(EventType.STARTED, ct2));
            eventList.Add(new Event(EventType.STARTED, ct3));


            Assert.IsFalse(SchedulerTests.IsCorrectOrder(eventList));
        }

        [TestMethod()]
        public void TestRule4() {

            var ct1 = new ColoredTask(Color.BLUE, () => { });
            var ct2 = new ColoredTask(Color.RED, () => { });
            var ct3 = new ColoredTask(Color.BLUE, () => { });


            var eventList = new EventList();
            eventList.Add(new Event(EventType.STARTED, ct3));
            eventList.Add(new Event(EventType.STARTED, ct2));
            eventList.Add(new Event(EventType.STARTED, ct1));


            Assert.IsFalse(SchedulerTests.IsCorrectOrder(eventList));
        }


        [TestMethod()]
        public void TestGood() {
            var ct1 = new ColoredTask(Color.BLUE, () => { });
            var ct2 = new ColoredTask(Color.RED, () => { });
            var ct3 = new ColoredTask(Color.BLUE, () => { });


            var eventList = new EventList();
            eventList.Add(new Event(EventType.STARTED, ct1));
            eventList.Add(new Event(EventType.STARTED, ct2));
            eventList.Add(new Event(EventType.FINISHED, ct1));
            eventList.Add(new Event(EventType.STARTED, ct1));
            eventList.Add(new Event(EventType.FINISHED, ct2));
            eventList.Add(new Event(EventType.FINISHED, ct1));


            Assert.IsTrue(SchedulerTests.IsCorrectOrder(eventList));
        }
    }
}
