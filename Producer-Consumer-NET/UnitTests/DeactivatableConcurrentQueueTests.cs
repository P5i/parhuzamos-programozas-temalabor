﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Producer_Consumer_NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Producer_Consumer_NET.Tests {
    [TestClass()]
    public class DeactivatableConcurrentQueueTests {

        private DeactivatableConcurrentQueue<ColoredTask> queue;

        private ColoredTask test1 = new ColoredTask(Color.BLUE, () => { });
        private ColoredTask test2 = new ColoredTask(Color.RED, () => { });

        // Az alap logikáját teszteli -> queue-e?, activate?, üres?
        [TestMethod()] 
        public void SingleThreadBasic() {
            //Prepare
            queue = new DeactivatableConcurrentQueue<ColoredTask>();
            queue.Enqueue(test1);
            queue.Enqueue(test2);

            Assert.IsTrue(queue.Activated);

            // First dequeue
            var ct1 = queue.Dequeue();
            Assert.AreEqual(test1, ct1);

            // Activated?
            Assert.IsFalse(queue.Activated); 

            // Not activated -> null
            var ctNull = queue.Dequeue();
            Assert.IsNull(ctNull);

            // Peek the top -> test2
            var ctPeeked = queue.Peek();
            Assert.AreEqual(test2, ctPeeked);

            queue.Activate();
            Assert.IsTrue(queue.Activated);

            // Ativated -> get test2
            var ct2 = queue.Dequeue();
            Assert.AreEqual(test2, ct2);

            // IsEmpty?
            Assert.AreEqual(0, queue.Size());
            Assert.IsNull(queue.Peek());
            
            // Put back -> peeked should be test2
            queue.PutBack();
            Assert.AreEqual(test2, queue.Peek());
        }

        // Több szálból teszteli a queue logikáját
        [TestMethod()]
        public void Multithread() {
            // Prepare
            queue = new DeactivatableConcurrentQueue<ColoredTask>();
            queue.Enqueue(test1);
            queue.Enqueue(test2);

            ColoredTask ct1 = null;
            var th1 = new Thread(() => {
                ct1 = queue.Dequeue();
                Thread.Sleep(1000);
                queue.Activate();
            });

            ColoredTask ct2 = null;
            var th2 = new Thread(() => {
                ct2 = queue.Dequeue();
            });

            ColoredTask ct3 = null;
            var th3 = new Thread(() => {
                ct3 = queue.Dequeue();
            });

            th1.Start();
            th2.Start();
            th1.Join();
            th3.Start();
            th2.Join();
            th3.Join();

            Assert.AreEqual(test1, ct1);
            Assert.IsNull(ct2);
            Assert.AreEqual(test2, ct3);
        }
    }
}