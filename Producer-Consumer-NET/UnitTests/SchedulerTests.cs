﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Producer_Consumer_NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Producer_Consumer_NET.Tests {
    [TestClass()]
    public class SchedulerTests {
        
        public enum EventType {
            STARTED, 
            FINISHED
        }

        public class Event {
            public EventType Type { get; private set; }
            public ColoredTask Task { get; private set; }

            public Event(EventType type, ColoredTask ct) {
                this.Type = type;
                this.Task = ct;
            }
        }

        public class EventList {
            private List<Event> events = new List<Event>();
            private readonly object eventSync = new object();

            public int Size {
                get => events.Count;
            }

            public Event this[int index] {
                get => events[index];
            }

            public void Add(Event e) {
                lock (eventSync) {
                    events.Add(e);
                }                
            } 
        }        

        private void Log(EventList list, Event e) {
            list.Add(e);
        }

        static public bool IsCorrectOrder(EventList list) {
            for (int i = 0; i < list.Size; i++) {

                var currEvent = list[i];

                if (currEvent.Type == EventType.FINISHED) {
                    continue;
                }

                bool finished = false;
                for (int j = i + 1; j < list.Size; j++) {
                    var e = list[j];                    

                    // 1. Egy feladatot csak egy végrehajtónak szabad kiadni.
                    if (!finished && e.Task.ID == currEvent.Task.ID && e.Type == EventType.STARTED) {
                        return false;
                    }

                    // 3. Az egész rendszerben egy adott színű feladatból egy időben csak egy lehet végrehajtás alatt.
                    if (!finished && e.Task.Color == currEvent.Task.Color && e.Type == EventType.STARTED) {
                        return false;
                    }

                    // Egy adott színhez tartozó feladatokat a beadás sorrendjében kell végrehajtani.
                    if (finished && e.Task.Color == currEvent.Task.Color && currEvent.Task.ID > e.Task.ID) {
                        return false;
                    }

                    // Meg van a befejezése
                    if (!finished && e.Task.ID == currEvent.Task.ID && e.Type == EventType.FINISHED) {
                        finished = true;
                        continue;
                    }
                }
            }
            return true;
        }

        // Egyszerűsíti a teszt esetek meadását
        private enum TaskSize {
            SMALL = 35, 
            MEDIUM = 37,
            LARGE = 39
        }

        // A teszt adatok miatt kell új Producer
        private void TestProducer(Scheduler scheduler, List<ColoredTask> tasks) {
            foreach (var t in tasks) {
                scheduler.AddTask(t);
            }
        }
        
        
        private void TaskWrapper(int n, ColoredTask task, EventList list, AutoResetEvent are, ref int counter) {
            Log(list, new Event(EventType.STARTED, task));
            Program.FibonacciTask(n);
            Log(list, new Event(EventType.FINISHED, task));
            if (++counter == 5) {
                are.Set();
            }
        }

        // KÉK pici -> KÉK pici -> ZÖLD közepes -> PIROS pici -> KÉK pici
        [TestMethod()]
        public void TestCaseExample() {
            var testTasks = new List<ColoredTask>() {
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.GREEN),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.BLUE),
            };

            var eventList = new EventList();

            // ha mind az 5 befejeződőtt -> kell mert Monitor.Wait()-ben vannak a threadek -> nem lehet Join
            var are = new AutoResetEvent(false);

            int counter = 0;

            testTasks[0].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[0], eventList, are, ref counter); };
            testTasks[1].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[1], eventList, are, ref counter); };
            testTasks[2].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[2], eventList, are, ref counter); };
            testTasks[3].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[3], eventList, are, ref counter); };
            testTasks[4].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[4], eventList, are, ref counter); };            

            // Most 3 különböző szín van
            var scheduler = new Scheduler(3);

            var producerTestThread = new Thread(() => TestProducer(scheduler, testTasks));
            producerTestThread.Start();

            // Tuti belerakja
            Thread.Sleep(10);

            int costimerThreadCnt = 2;
            var consumerThreads = new List<Thread>(costimerThreadCnt);
            for (int i = 0; i < costimerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Program.Consumer(scheduler)));
                consumerThreads[i].Start();
            }

            are.WaitOne();
            Assert.IsTrue(SchedulerTests.IsCorrectOrder(eventList));
        }


        // 1 thread, 1 color
        [TestMethod]
        public void TestCase1() {
            var testTasks = new List<ColoredTask>() {
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
            };

            var eventList = new EventList();

            // ha mind az 5 befejeződőtt -> kell mert Monitor.Wait()-ben vannak a threadek -> nem lehet Join
            var are = new AutoResetEvent(false);

            int counter = 0;

            testTasks[0].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[0], eventList, are, ref counter); };
            testTasks[1].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[1], eventList, are, ref counter); };
            testTasks[2].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[2], eventList, are, ref counter); };
            testTasks[3].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[3], eventList, are, ref counter); };
            testTasks[4].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[4], eventList, are, ref counter); };
            testTasks[5].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[5], eventList, are, ref counter); };
            testTasks[6].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[6], eventList, are, ref counter); };

            // Most 3 különböző szín van
            var scheduler = new Scheduler(3);

            var producerTestThread = new Thread(() => TestProducer(scheduler, testTasks));
            producerTestThread.Start();

            // Tuti belerakja
            Thread.Sleep(10);

            int costimerThreadCnt = 1;
            var consumerThreads = new List<Thread>(costimerThreadCnt);
            for (int i = 0; i < costimerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Program.Consumer(scheduler)));
                consumerThreads[i].Start();
            }

            are.WaitOne();
            Assert.IsTrue(SchedulerTests.IsCorrectOrder(eventList));
        }

        // 3 thread, 1 color
        [TestMethod]
        public void TestCase2() {
            var testTasks = new List<ColoredTask>() {
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.RED),
            };

            var eventList = new EventList();

            // ha mind az 5 befejeződőtt -> kell mert Monitor.Wait()-ben vannak a threadek -> nem lehet Join
            var are = new AutoResetEvent(false);

            int counter = 0;

            testTasks[0].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[0], eventList, are, ref counter); };
            testTasks[1].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[1], eventList, are, ref counter); };
            testTasks[2].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[2], eventList, are, ref counter); };
            testTasks[3].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[3], eventList, are, ref counter); };
            testTasks[4].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[4], eventList, are, ref counter); };
            testTasks[5].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[5], eventList, are, ref counter); };
            testTasks[6].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[6], eventList, are, ref counter); };

            // Most 3 különböző szín van
            var scheduler = new Scheduler(3);

            var producerTestThread = new Thread(() => TestProducer(scheduler, testTasks));
            producerTestThread.Start();

            // Tuti belerakja
            Thread.Sleep(10);

            int costimerThreadCnt = 3;
            var consumerThreads = new List<Thread>(costimerThreadCnt);
            for (int i = 0; i < costimerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Program.Consumer(scheduler)));
                consumerThreads[i].Start();
            }

            are.WaitOne();
            Assert.IsTrue(SchedulerTests.IsCorrectOrder(eventList));
        }

        // 2 thread, 2 color
        [TestMethod]
        public void TestCase3() {
            var testTasks = new List<ColoredTask>() {
                new ColoredTask(Color.RED),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.RED),
            };

            var eventList = new EventList();

            // ha mind az 5 befejeződőtt -> kell mert Monitor.Wait()-ben vannak a threadek -> nem lehet Join
            var are = new AutoResetEvent(false);

            int counter = 0;

            testTasks[0].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[0], eventList, are, ref counter); };
            testTasks[1].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[1], eventList, are, ref counter); };
            testTasks[2].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[2], eventList, are, ref counter); };
            testTasks[3].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[3], eventList, are, ref counter); };
            testTasks[4].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[4], eventList, are, ref counter); };
            testTasks[5].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[5], eventList, are, ref counter); };
            testTasks[6].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[6], eventList, are, ref counter); };

            // Most 3 különböző szín van
            var scheduler = new Scheduler(3);

            var producerTestThread = new Thread(() => TestProducer(scheduler, testTasks));
            producerTestThread.Start();

            // Tuti belerakja
            Thread.Sleep(10);

            int costimerThreadCnt = 2;
            var consumerThreads = new List<Thread>(costimerThreadCnt);
            for (int i = 0; i < costimerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Program.Consumer(scheduler)));
                consumerThreads[i].Start();
            }

            are.WaitOne();
            Assert.IsTrue(SchedulerTests.IsCorrectOrder(eventList));
        }

        // 5 thread, 3 color
        [TestMethod]
        public void TestCase4() {
            var testTasks = new List<ColoredTask>() {
                new ColoredTask(Color.GREEN),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.BLUE),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.GREEN),
                new ColoredTask(Color.RED),
                new ColoredTask(Color.GREEN),
            };

            var eventList = new EventList();

            // ha mind az 5 befejeződőtt -> kell mert Monitor.Wait()-ben vannak a threadek -> nem lehet Join
            var are = new AutoResetEvent(false);

            int counter = 0;

            testTasks[0].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[0], eventList, are, ref counter); };
            testTasks[1].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[1], eventList, are, ref counter); };
            testTasks[2].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[2], eventList, are, ref counter); };
            testTasks[3].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[3], eventList, are, ref counter); };
            testTasks[4].Function = () => { TaskWrapper((int)TaskSize.LARGE, testTasks[4], eventList, are, ref counter); };
            testTasks[5].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[5], eventList, are, ref counter); };
            testTasks[6].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[6], eventList, are, ref counter); };
            testTasks[7].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[7], eventList, are, ref counter); };
            testTasks[8].Function = () => { TaskWrapper((int)TaskSize.SMALL, testTasks[8], eventList, are, ref counter); };
            testTasks[9].Function = () => { TaskWrapper((int)TaskSize.MEDIUM, testTasks[9], eventList, are, ref counter); };

            // Most 3 különböző szín van
            var scheduler = new Scheduler(3);

            var producerTestThread = new Thread(() => TestProducer(scheduler, testTasks));
            producerTestThread.Start();

            // Tuti belerakja
            Thread.Sleep(10);

            int costimerThreadCnt = 5;
            var consumerThreads = new List<Thread>(costimerThreadCnt);
            for (int i = 0; i < costimerThreadCnt; i++) {
                consumerThreads.Add(new Thread(() => Program.Consumer(scheduler)));
                consumerThreads[i].Start();
            }

            are.WaitOne();
            Assert.IsTrue(SchedulerTests.IsCorrectOrder(eventList));
        }
    }
}