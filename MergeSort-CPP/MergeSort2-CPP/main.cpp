#include <iostream>
#include <chrono>
#include <algorithm>
#include <iterator>
#include <random>
#include <functional>

#include <thread>

#include <ppl.h>


template <typename Fun>
auto measureTime(Fun fun) {
	auto start = std::chrono::high_resolution_clock::now();
	fun();
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsedTime = end - start;
	auto d = std::chrono::duration_cast<std::chrono::microseconds>(elapsedTime);
	return d.count();
}

namespace naiveMergeSort {
	void merge(int *in, int begin, int mid, int end, int *out) {
		int i = begin, j = mid;
		for (int c = begin; c < end; ++c) {
			if (i < mid && (j >= end || in[i] <= in[j])) {
				out[c] = in[i];
				i++;
			} else {
				out[c] = in[j];
				j++;
			}
		}
	}

	void copy(int *in, int begin, int end, int *out) {
		for (int c = begin; c < end; ++c) {
			out[c] = in[c];
		}
	}

	void mergeSort(int *tomb, int begin, int end, int *temp) {
		if (end - begin < 2)
			return;
		int mid = (begin + end) / 2;
		mergeSort(tomb, begin, mid, temp);
		mergeSort(tomb, mid, end, temp);
		merge(tomb, begin, mid, end, temp);
		copy(temp, begin, end, tomb);
	}

	void merge_sort(int *array, int begin, int end) {
		int* temp = new int[end - begin];
		mergeSort(array, begin, end, temp);
		delete[] temp;
	}
}

namespace modernMergeSort {
	template <typename T>
	void merge(T *in, int begin, int mid, int end, T *out) {
		int i = begin, j = mid;
		for (int c = begin; c < end; ++c) {
			if (i < mid && (j >= end || in[i] <= in[j])) {
				new (&out[c]) T{std::move(in[i])};
				i++;
			}
			else {
				new (&out[c]) T{std::move(in[j])};
				j++;
			}
		}
	}

	template <typename T>
	void copy(T *in, int begin, int end, T *out) {
		for (int c = begin; c < end; ++c) {
			out[c] = std::move(in[c]);
			in[c].~T();
		}
	}

	template <typename T>
	void mergeSort(T *tomb, int begin, int end, T *temp) {
		if (end - begin < 2)
			return;
		int mid = (begin + end) / 2;
		mergeSort(tomb, begin, mid, temp);
		mergeSort(tomb, mid, end, temp);
		merge(tomb, begin, mid, end, temp);
		copy(temp, begin, end, tomb);
	}

	template <typename T>
	void mergeSort(T *tomb, int begin, int end)	{
		T* temp = static_cast<T*>(::operator new(sizeof(T) * (end - begin)));
		mergeSort(tomb, begin, end, temp);
		::operator delete(temp);
	}
}

namespace parallelMergeSort {
	void merge(int *in, int begin, int mid, int end, int *out) {
		int i = begin, j = mid;
		for (int c = begin; c < end; ++c) {
			if (i < mid && (j >= end || in[i] <= in[j])) {
				out[c] = in[i];
				i++;
			} else {
				out[c] = in[j];
				j++;
			}
		}
	}

	void copy(int *in, int begin, int end, int *out) {
		for (int c = begin; c < end; ++c) {
			out[c] = in[c];
		}
	}


	void mergeSort(int *array, int begin, int end, int *temp, int threshold) {
		if (end - begin < 2)
			return;
		int mid = (begin + end) / 2;

		if (end - begin < threshold) {
			mergeSort(array, begin, mid, temp, threshold);
			mergeSort(array, mid, end, temp, threshold);
		}
		else {
				concurrency::parallel_invoke(
				[&]() { mergeSort(array, begin, mid, temp, threshold); },
				[&]() { mergeSort(array, mid, end, temp, threshold); }	
			);
		}
		merge(array, begin, mid, end, temp);
		copy(temp, begin, end, array);
	}

	void mergeSort(int *array, int begin, int end, int threshold = 975) {
		int* temp = new int[end - begin];
		mergeSort(array, begin, end, temp, threshold);
		delete[] temp;
	}


}

namespace stlMergeSort {

	template<typename Iter>
	void mergeSort(Iter first, Iter last)	{
		if (last - first > 1) {
			Iter middle = first + (last - first) / 2;
			mergeSort(first, middle);
			mergeSort(middle, last);
			std::inplace_merge(first, middle, last);
		}
	}
}

namespace inplaceMergeSort {
	void merge(int array[], int begin, int middle, int end) {
		int i = begin;
		int j = middle + 1;

		while (i != end && j != end) {
			if (array[i] > array[j]) {
				int temp = array[j];
				for (int k = j; k > i; k--) {
					array[k] = array[k - 1];
				}
				array[i] = temp;
				i++;
				j++;
			}
			else {
				i++;
			}
		}
	}

	void mergeSort(int *array, int begin, int end, int *temp) {
		if (end - begin < 2)
			return;
		int mid = (begin + end) / 2;
		mergeSort(array, begin, mid, temp);
		mergeSort(array, mid, end, temp);
		merge(array, begin, mid, end);
	}

	void mergeSort(int *array, int begin, int end) {
		int* temp = new int[end - begin];
		mergeSort(array, begin, end, temp);
		delete[] temp;
	}
}

void compareAlgorithms() {

	int sizes[] = {426900, 694200, 1690888, 4888888, 11568782, 69900690};

	for (auto size : sizes) {
		std::cout << size << std::endl;

		int* array = new int[size];

		std::mt19937_64 rng{std::random_device{}()};
		std::uniform_int_distribution<int> d{0, 2000};
		std::generate(array, array + size, std::bind(d, rng));

		for (int i = 0; i < 10; i++) {
			std::generate(array, array + size, std::bind(d, rng));
			std::cout << measureTime([&](){
				naiveMergeSort::merge_sort(array, 0, size);
			}) << " ";
		}

		std::cout << std::endl;

		for (int i = 0; i < 10; i++) {
			std::generate(array, array + size, std::bind(d, rng));
			std::cout << measureTime([&](){
				modernMergeSort::mergeSort(array, 0, size);
			}) << " ";
		}

		std::cout << std::endl;

		for (int i = 0; i < 10; i++) {
			std::generate(array, array + size, std::bind(d, rng));
			std::cout << measureTime([&](){
				stlMergeSort::mergeSort(array, array +  size);
			}) <<" ";
		}

		std::cout << std::endl;

		for (int i = 0; i < 10; i++) {
			std::generate(array, array + size, std::bind(d, rng));
			std::cout << measureTime([&](){
				parallelMergeSort::mergeSort(array, 0, size);
			}) <<" ";
		}

		std::cout << std::endl << std::endl;
	}
}

void testThreshold() {
	int size = 1000000;
	int* array = new int[size];
	std::mt19937_64 rng{std::random_device{}()};
	std::uniform_int_distribution<int> d{0, 2000};

	for (int threashold = 10; threashold < 1000000; ) {
		std::cout << threashold << " ";
		if (threashold < 1000) {
			threashold++;
		}
		else {
			threashold += 2500;
		}
	}
	std::cout << std::endl;

	for (int i = 0; i < 1; i++) {
		for (int threashold = 10; threashold < 1000000; ) {
			std::generate(array, array + size, std::bind(d, rng));
			std::cout << measureTime([&](){
				parallelMergeSort::mergeSort(array, 0, size, threashold);
			}) <<" ";

			if (threashold < 1000) {
				threashold++;
			}
			else {
				threashold += 2500;
			}
		}
		std::cout << std::endl;
	}
}


int main() {

	testThreshold();

	//getchar();
	return 0;
}