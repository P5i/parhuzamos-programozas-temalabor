# 4. feladat: Merge-sort C++ nyelven
  
A méréseket egy Intel Core i7-6700HQ CPU-n végeztem.   
- alapsebessége: 2.6 GHz, de a méréseken elérte a 3.3 GHz is, az átlaga ~3.15 GHz volt  
- magok száma: 4  
- logikai magok száma: 8  
  

## Parallel merge-sort threshold értékének meghatározása

A mérést úgy végeztem, hogy az algoritmus lefuttattam egy 1 000 000 méretű tömbre sok különböző értékkel. 1-1000 között egyesével néztem, mert a várt eredmény ebben az intervallumban volt. Utána 2500-sával növeltem a threshold értékét. 
Ezt 10x futtatam le, majd a 2 legkisebbet és legnagyobbat elhagytam, a többit pedig átlagoltam. A méréshez a Stopwatch ElapsedTicks-t használtam mértékegységként.

Az eredmény láthatóan ~250 000-nél lenne, ezt valószínűleg a sok másolás okozza. Hiába próbálkoztam inplace merge-sorttal a végeredmény csak rosszabb lett. A merge 
O(n<sup>2</sup>) futásidejű, viszont O(1) memória szempontjából. De nem így nem marad nlog(n). 

![](images/F4_threshold_1-1000.png)

![](images/F4_threshold_all.png)

  
## Egyszálú és párhuzamos merge-sort összehasonlítása

A mérést a következő méretű tömbökkel végeztem: 426 900, 694 200, 1 690 888, 4 888 888, 11 568 782, 69 900 690

Az egyszálú (naív, modern, STL) és párhuzamos algoritmusokon 10x futtatam le, majd a 2 legkisebbet és legnagyobbat elhagytam, a többit pedig átlagoltam.

### Az eredmények
![](images/F4_meres_small.png)

![](images/F4_meres_big.png)
