﻿using System;using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

 

namespace MergeSort_NET {

    internal static class Program {
        public static int Main(string[] args) {
            
            Test();

            return 0;
            
            if (args.Length != 2) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter a size and algorithm's name!");
                return 1;
            }

            if (!int.TryParse(args[0], out int size)) // Try-pattern
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter an integer for the size of the array!");
                return 1;
            }

            var random = new Random();
            int[] array = Enumerable
                .Range(0, size)
                .Select(i => random.Next(-10000, 10000))
                .ToArray();

            string algName = args[1];

            switch (args[1]) {
                case "single": {
                    using (new Timer("Single thread merge sor: ")) {
                        SingleThreadMergeSort.MergeSort(array);
                    }
                }
                    break;

                case "parallel": {
                    using (new Timer("Parallel thread merge sor: ")) {
                        ParallelMergeSort.MergeSort(array, 100);
                    }
                }
                    break;

                default: {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Please enter an existing algorithm's name!");
                    return 1;
                }
            }

            foreach (var item in array) {
                Console.WriteLine($"{item} ");
            }

            return 0;
        }

        private static void Test() {
            int[] sizes = {426_900, 694_200, 1_690_888, 4_888_888, 11_568_782, 69_900_690};
            var random = new Random();
                
            foreach (int size in sizes) {
                Console.WriteLine($"\n{size}");

                for (int ii = 0; ii < 10; ii++) {
                    int[] array = Enumerable
                        .Range(0, size)
                        .Select(i => random.Next(-10000, 10000))
                        .ToArray();

                    using (new Timer("Single thread merge sort: ", true)) {
                        SingleThreadMergeSort.MergeSort(array);
                    }
                }

                Console.WriteLine();
                
                for (int ii = 0; ii < 10; ii++) {
                    int[] array = Enumerable
                        .Range(0, size)
                        .Select(i => random.Next(-10000, 10000))
                        .ToArray();

                    using (new Timer("Parallel merge-sort", true)) {
                        ParallelMergeSort.MergeSort(array);
                    }
                }
            }
        }
       

        public static void TestThreshold() {
            var random = new Random();
            
            for (int j = 10; j < 1_000_000;) {
                Console.Write($"{j} ");

                if (j < 1000) {
                    j++;
                }
                else {
                    j += 2500;
                }
            }
            Console.WriteLine();
            
            for (int ii = 0; ii < 10; ii++) {

                for (int j = 10; j < 1_000_000;) {
                    int[] testArray = Enumerable
                        .Range(0, 1_000_000)
                        .Select(i => random.Next(-10000, 10000))
                        .ToArray();

                    using (new Timer("Parallel thread merge sor: ", true)) {
                        ParallelMergeSort.MergeSort(testArray, j);
                    }

                    if (j < 1000) {
                        j++;
                    }
                    else {
                        j += 2500;
                    }
                }
                
                Console.WriteLine();

            }
            
            Console.ReadKey(); 
        }
    }
}