using System;
using System.Linq;

// https://stackoverflow.com/questions/1389821/array-copy-vs-buffer-blockcopy
// https://stackoverflow.com/questions/406485/array-slices-in-c-sharp
namespace MergeSort_NET {
    public static class Copy {
        public static void Naive(int[] source, int begin, int end, int[] destination) {
            for (int i = begin; i < end; i++) {
                destination[i] = source[i];
            }
        }

        public static void Array(int[] source, int begin, int end, int[] destination) {
            System.Array.Copy(source, destination, end - begin);
        }

        public static void LINQ(int[] source, int begin, int end, int[] destination) {
            destination = source
                            .Skip(begin)
                            .Take(end)
                            .ToArray();
        }

        public static void Buffered(int[] source, int begin, int end, int[] destination) {
            Buffer.BlockCopy(source, begin * sizeof(int), destination, begin * sizeof(int), end - begin);
        }

        public static void Test() {
            int[] a1 = new int[9999999];
            int[] a2 = new int[999999999];
            
            var random = new Random();
            a1 = a1.AsEnumerable()
                .Select(_ => random.Next(0, 100000))
                .ToArray();

            using (new Timer("Copy: Naive")) {
                Copy.Naive(a1, 0, a1.Length, a2);
            }
            using (new Timer("Copy: LINQ")) {
                Copy.LINQ(a1, 0, a1.Length, a2);
            }
            using (new Timer("Copy: Array")) {
                Copy.Array(a1, 0, a1.Length, a2);
            }
            using (new Timer("Copy: Buffered")) {
                Copy.Buffered(a1, 0, a1.Length, a2);
            }
        }
    }
}