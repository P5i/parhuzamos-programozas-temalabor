namespace MergeSort_NET {
    
    public static class SingleThreadMergeSort {

        private static void Merge(int[] inArray, int begin, int middle, int end, int[] outArray) {
            int i = begin, j = middle;
            for (int index = i; index < end; index++) {
                if (i < middle && (j >= end || inArray[i] <= inArray[j])) {
                    outArray[index] = inArray[i++];
                }
                else {
                    outArray[index] = inArray[j++];
                }
            }
        }

        private static void MergeSort(int[] array, int begin, int end, int[] temp) {
            if (end - begin < 2)
                return;
            
            int middle = (begin + end) / 2;
            MergeSort(array, begin, middle, temp);
            MergeSort(array, middle, end, temp);
            Merge(array, begin, middle, end, temp);
            //Buffer.BlockCopy(temp, begin * sizeof(int), array, begin * sizeof(int), end - begin); -> nem működik
            Copy(temp, begin, end, array);
        }

        private static void Copy(int[] source, int begin, int end, int[] destination) {
            for (int i = begin; i < end; i++) {
                destination[i] = source[i];
            }
        }

        public static void MergeSort(int[] array, int begin, int end) {
            var temp = new int[end - begin]; // kevesebb memória foglalás!
            MergeSort(array, begin, end, temp);
        }

        public static void MergeSort(int[] array) {
            MergeSort(array, 0, array.Length);
        }

    }
}