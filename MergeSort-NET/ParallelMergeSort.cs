using System;
using System.Threading;
using System.Threading.Tasks;

namespace MergeSort_NET {
    public static class ParallelMergeSort {

        private static void Merge(int[] inArray, int begin, int middle, int end, int[] outArray) {
            int i = begin, j = middle;
            for (int index = i; index < end; index++) {
                if (i < middle && (j >= end || inArray[i] <= inArray[j])) {
                    outArray[index] = inArray[i++];
                }
                else {
                    outArray[index] = inArray[j++];
                }
            }
        }

        private static void MergeSort(int[] array, int begin, int end, int[] temp, int threshold = 975) { /*1_000_000/2^10*/ 
            if (end - begin < 2)
                return;
            
            int middle = (begin + end) / 2;

            if (end - begin < threshold) {
                MergeSort(array, begin, middle, temp, threshold);
                MergeSort(array, middle, end, temp, threshold);
                //Array.Sort(array, begin, end - begin);
            }
            else {
                Parallel.Invoke(
                    () => {
                        MergeSort(array, begin, middle, temp, threshold);
                    },
                    () => {
                        MergeSort(array, middle, end, temp, threshold);
                    } 
                );
            }
            Merge(array, begin, middle, end, temp);
            Copy(temp, begin, end, array);
        }

        private static void Copy(int[] source, int begin, int end, int[] destination) {
            for (int i = begin; i < end; i++) {
                destination[i] = source[i];
            }
        }

        public static void MergeSort(int[] array, int begin, int end, int threshold = 975) {
            var temp = new int[end - begin]; // kevesebb memória foglalás!
            MergeSort(array, begin, end, temp, threshold);
        }

        public static void MergeSort(int[] array, int threshold = 975) {
            MergeSort(array, 0, array.Length, threshold);
        }

        public static void TestInvoke() {
            Parallel.Invoke(
                () => 
                {
                    for (int i = 0; i < 1000; i++) {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($"{i} ");
                    }
                },
                () => 
                {
                    for (int i = 0; i < 1000; i++) {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"{i} ");
                    }
                },
                () => 
                {
                    for (int i = 0; i < 1000; i++) {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write($"{i} ");
                    }
                }
            );
        }
        
    }
}