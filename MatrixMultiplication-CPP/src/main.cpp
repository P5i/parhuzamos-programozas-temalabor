#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <cctype>
#include <functional>
#include <chrono>
#include <thread>
#include <map>

#include "Matrix.h"

#define COMPARE 1

using std::cout;
using std::endl;

template <typename Fun>
auto measureTime(Fun fun)
{
    auto start = std::chrono::high_resolution_clock::now();
    fun();
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsedTime = end - start;
    auto d = std::chrono::duration_cast<std::chrono::milliseconds>(elapsedTime);
    return d.count();
}

template <std::size_t N, typename Fun>
void doMeasurement(char const* prefix, Fun fun, char sep = '\n') {
    auto mtx1 = Matrix<N>::createRandom();
    auto mtx2 = Matrix<N>::createRandom();

#if COMPARE
    for (std::size_t i = 0; i < 10; i++)
    {
#endif
        auto dt = measureTime([&] {
            fun(mtx1, mtx2);
        });
#if COMPARE
        std::cout << dt << sep;
    }
    cout << endl;
#else
        std::cout << prefix << '(' << N << 'x' << N << "): " << dt << " ticks." << std::endl;
#endif
}


void compareAlgs()
{
    constexpr std::size_t sizes[] = { 100, 300, 420, 696, 1000, 2000 };

    auto testMtx1 = Matrix<sizes[0]>::createRandom();
    auto testMtx2 = Matrix<sizes[0]>::createRandom();

    cout << endl << "Test size " << sizes[0] << endl;
    doMeasurement<sizes[0]>("", multiply_naiveColumnMajor<sizes[0]>, ',');
    doMeasurement<sizes[0]>("", multiply_naiveRowMajor<sizes[0]>, ',');
    doMeasurement<sizes[0]>("", multiply_ThreadPool<sizes[0]>, ',');
    doMeasurement<sizes[0]>("", multiply_OpenMP<sizes[0]>, ',');

    cout << endl << "Test size " << sizes[1] << endl;
    doMeasurement<sizes[1]>("", multiply_naiveColumnMajor<sizes[1]>, ',');
    doMeasurement<sizes[1]>("", multiply_naiveRowMajor<sizes[1]>, ',');
    doMeasurement<sizes[1]>("", multiply_ThreadPool<sizes[1]>, ',');
    doMeasurement<sizes[1]>("", multiply_OpenMP<sizes[1]>, ',');

    cout << endl << "Test size " << sizes[2] << endl;
    doMeasurement<sizes[2]>("", multiply_naiveColumnMajor<sizes[2]>, ',');
    doMeasurement<sizes[2]>("", multiply_naiveRowMajor<sizes[2]>, ',');
    doMeasurement<sizes[2]>("", multiply_ThreadPool<sizes[2]>, ',');
    doMeasurement<sizes[2]>("", multiply_OpenMP<sizes[2]>, ',');

    cout << endl << "Test size " << sizes[3] << endl;
    doMeasurement<sizes[3]>("", multiply_naiveColumnMajor<sizes[3]>, ',');
    doMeasurement<sizes[3]>("", multiply_naiveRowMajor<sizes[3]>, ',');
    doMeasurement<sizes[3]>("", multiply_ThreadPool<sizes[3]>, ',');
    doMeasurement<sizes[3]>("", multiply_OpenMP<sizes[3]>, ',');

    cout << endl << "Test size " << sizes[4] << endl;
    doMeasurement<sizes[4]>("", multiply_naiveColumnMajor<sizes[4]>, ',');
    doMeasurement<sizes[4]>("", multiply_naiveRowMajor<sizes[4]>, ',');
    doMeasurement<sizes[4]>("", multiply_ThreadPool<sizes[4]>, ',');
    doMeasurement<sizes[4]>("", multiply_OpenMP<sizes[4]>, ',');

    /*cout << endl << "Test size " << sizes[5] << endl;
    doMeasurement<sizes[5]>("", multiply_naiveColumnMajor<sizes[5]>, ',');
    doMeasurement<sizes[5]>("", multiply_naiveRowMajor<sizes[5]>, ',');
    doMeasurement<sizes[5]>("", multiply_ThreadPool<sizes[5]>, ',');
    doMeasurement<sizes[5]>("", multiply_OpenMP<sizes[5]>, ',');*/

    //TODO vhogy egy forban?

}

int main(int argc, char* args[])
{
    compareAlgs();
    return 0;

    auto mtx1 = Matrix<4>
            {
                    1, 2, 3, 4,
                    5, 6, 7, 8,
                    9, 10, 11, 12,
                    13, 14, 15, 16
            };

    auto mtx2 = Matrix<4>
            {
                    16, 15, 14, 13,
                    12, 11, 10, 9,
                    8, 7, 6, 5,
                    4, 3, 2, 1
            };

    cout << multiply_naiveColumnMajor(mtx1, mtx2) << endl;
    cout << multiply_naiveRowMajor(mtx1, mtx2) << endl;
    cout << multiply_OpenMP(mtx1, mtx2) << endl;
    cout << multiply_ThreadPool(mtx1, mtx2) << endl;

    cout << Matrix<5>::createRandom() << endl;
    //cout << Matrix<5>::createRandom() << endl;
    //cout << Matrix<5>::createRandom() << endl;
    //cout << Matrix<5>::createRandom() << endl;

    //doMeasurement<512>("Naive Column Major", multiply_naiveColumnMajor<512>);

    constexpr std::size_t i = 100;

    doMeasurement<i>("OpenMP", multiply_OpenMP<i>);
    doMeasurement<2000>("ThreadPool", multiply_ThreadPool<2000>);



    return 0;
    // argv[0] is the pointer to the initial character of a null-terminated multibyte
    // string that represents the name used to invoke the program itself
    // or an empty string (if not supported)
    if (argc != 2 + 1)
    {
        // std::cerr az output elejére írja
        std::cout << "Please enter a size and algorithm's name!" << endl;
        return 1;
    }

    int mtxSize = 0;
    try
    {
        // Ha 40a0-t ír -> 40 lesz, ha az elején van nem szám akkor dob csak kivételt
        mtxSize = std::stoi(args[1]);
    }
    catch (std::invalid_argument& e)
    {
        std::cout << "Please enter an integer for the size!";
        return 1;
    }
    cout << mtxSize << endl;

    int num = 0;

    return 0;
}