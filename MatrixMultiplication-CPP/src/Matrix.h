#pragma once

#include <array>
#include <cstdint>
#include <initializer_list>
#include <iomanip>
#include <iostream>
#include <memory>
#include <random>
#include <omp.h>
#include <functional>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>

template <std::size_t N>
class Matrix
{
private:
    using array_t = std::array<int, N * N>;
    std::unique_ptr<array_t> elements = std::make_unique<array_t>(); // A heapre kell allokálni

public:
    Matrix() = default;
    Matrix(Matrix&&) = default;
    Matrix& operator=(Matrix&&) = default;

    Matrix& operator=(Matrix const&) = delete;
    Matrix(Matrix const&) = delete;

    Matrix(std::initializer_list<int> elems)
    {
        std::copy(std::begin(elems), std::end(elems), std::begin(*elements));
    }

    int& operator()(std::size_t i, std::size_t j)
    {
        return elements->operator[](i * N + j);
    }

    int const& operator()(std::size_t i, std::size_t j) const
    {
        return elements->operator[](i * N + j);
    }

    array_t& array()
    {
        return *elements.get();
    }

    array_t const& array() const
    {
        return *elements.get();
    }

    static Matrix createRandom()
    {
        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_real_distribution<> dist(-1000, 1000);

        auto result = Matrix<N>();
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                result(i, j) = dist(mt);

        return result;
    };
};

template <std::size_t N>
bool operator==(Matrix<N> const& a, Matrix<N> const& b)
{
    return *a.elements == *b.elements;
}

template <std::size_t N>
std::ostream& operator<<(std::ostream& os, Matrix<N> const& mtx)
{
    for (std::size_t i = 0; i < N; i++)
    {
        for (std::size_t j = 0; j < N; j++)
            os << std::setw(5) << mtx(i, j) << " ";
        os << std::endl;
    }
    return os;
}

template <std::size_t N>
Matrix<N> multiply_naiveColumnMajor(Matrix<N> const& lhs, Matrix<N> const& rhs)
{
    auto result = Matrix<N>();

    for (std::size_t i = 0; i < N; i++)
        for (std::size_t j = 0; j < N; j++)
            for (std::size_t k = 0; k < N; k++)
                result(i, j) += lhs(i, k) * rhs(k, j);

    return result;
}

template <std::size_t N>
Matrix<N> multiply_naiveRowMajor(Matrix<N> const& lhs, Matrix<N> const& rhs)
{
    auto result = Matrix<N>();

    for (std::size_t j = 0; j < N; j++)
        for (std::size_t i = 0; i < N; i++)
            for (std::size_t k = 0; k < N; k++)
                result(i, j) += lhs(i, k) * rhs(k, j);

    return result;
}

class ThreadPool
{
    using function_t = std::function<void()>;
private:
    std::vector<std::thread> threads;
    std::mutex queueMutex;
    std::condition_variable condition;
    std::queue<function_t> jobs;
    bool running = true;

public:
    ThreadPool()
    {
        auto threadCnt = std::thread::hardware_concurrency();
        threads.reserve(threadCnt);

        for (std::size_t i = 0; i < threadCnt; i++)
        {
            threads.emplace_back([this] {
               for(;;)
               {
                   function_t job;

                   { // a lock a scope-ra hat
                       auto lock = std::unique_lock<std::mutex>(queueMutex);
                       condition.wait(lock, [this]
                       { return !running || !jobs.empty(); });

                       if (!running && jobs.empty())
                           return;

                       job = std::move(jobs.front());
                       jobs.pop();
                   }
                   job();
               }
            });
        }
    }

    ~ThreadPool()
    {
        {
            auto lock = std::unique_lock<std::mutex>(queueMutex);
            running = false;
        }
        condition.notify_all();
        for (auto& t : threads)
            t.join();
    }

    void addJob(function_t fun)
    {
        {
            auto lock = std::unique_lock<std::mutex>(queueMutex);
            jobs.push(fun);
        }
        condition.notify_one();
    }
};

template <std::size_t N>
Matrix<N> multiply_ThreadPool(Matrix<N> const& lhs, Matrix<N> const& rhs)
{
    auto result = Matrix<N>();

    {
        auto pool = ThreadPool();
        for (std::size_t j = 0; j < N; j++)
        {
            pool.addJob([j, &lhs, &rhs, &result] {
                for (std::size_t i = 0; i < N; i++)
                    for (std::size_t k = 0; k < N; k++)
                        result(i, j) += lhs(i, k) * rhs(k, j);
            });
        }
    }

    return result;
}

template <std::size_t N>
Matrix<N> multiply_OpenMP(Matrix<N> const& lhs, Matrix<N> const& rhs)
{
    auto result = Matrix<N>();

    #pragma omp parallel for
    for (std::size_t j = 0; j < N; j++)
        for (std::size_t i = 0; i < N; i++)
            for (std::size_t k = 0; k < N; k++)
                result(i, j) += lhs(i, k) * rhs(k, j);

    return result;
}