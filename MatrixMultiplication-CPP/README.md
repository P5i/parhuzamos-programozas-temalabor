# 1. feladat: mátrix szorzás C# nyelven
  
A méréseket egy Intel Core i7-6700HQ CPU-n végeztem.   
- alapsebessége: 2.6 GHz, de a méréseken elérte a 3.3 GHz is, az átlaga ~3.15 GHz volt  
- magok száma: 4  
- logikai magok száma: 8  

## Algoritmusok összehasonítása

A mérést a következő méretű mátrixokkal végeztem: 100, 300, 420, 696, 1000

Az összes algoritmuson 10x futtatam le, majd a 2 legkisebbet és legnagyobbat elhagytam, a többit pedig átlagoltam.

### Az eredmények
![](images/F2_diagram1.png)

![](images/F2_diagram2.png)
