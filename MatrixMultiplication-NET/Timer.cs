using System;
using System.Diagnostics;

namespace MatrixMultiplication_NET
{
    public class Timer : IDisposable
    {
        private string name;
        private Stopwatch sw;
        private bool printCSV;

        [ThreadStatic]
        private static int depth = 0;
        
        public Timer(string name, bool printCSV = false)
        {
            this.name = name;
            this.printCSV = printCSV;
            sw = Stopwatch.StartNew();
            depth++;

            if (!printCSV)
            {
                PrintDepth(">>");
                Console.WriteLine(this.name);
            }
            
        }

        public void Dispose()
        {
            sw.Stop();
            depth--;
            
            if (!printCSV)
            {
                PrintDepth("<<");
                Console.WriteLine($"{name} {sw.Elapsed}");
            }
            else
            {
                Console.Write($"{sw.ElapsedMilliseconds} ");    
            }
        }

        private static void PrintDepth(string sign)
        {
            for (int i = 0; i < depth - 1; i++)
                Console.Write("\t");
            Console.Write($"{sign} ");
        }
    }
}