# 1. feladat: mátrix szorzás C# nyelven
  
A méréseket egy Intel Core i7-6700HQ CPU-n végeztem.   
- alapsebessége: 2.6 GHz, de a méréseken elérte a 3.3 GHz is, az átlaga ~3.15 GHz volt  
- magok száma: 4  
- logikai magok száma: 8  
  
A kötelezőkön kívül még 2 algoritmust implementáltam. Az első (Multiply_SIMD) 1 magot használ, de közben kihasználja a a CPU   
vektorprocesszor tulajdonságát. A második (Multiply_TaskSIMD) már több magon használja ki ezt a TPL segítségével.

## Mikortól érdemes párhuzamosítani?
Arányszámként fogom használni a 'StopWatch' 'ElapsedTicks' propertyjét.  4x4-től 50x50 kiírom ezt.
 A mérésből a ~25x25-ös mátrixok jönnek ki eredményül. Tehát innentől lesz kevesebb a plusz overheadje a párhuzamosításnak.
 
 https://gitlab.com/P5i/parhuzamos-programozas-temalabor/commit/52ce9df55081b34ee9ed821a3d3e753122ae1ab3

## Algoritmusok összehasonítása

A mérést a következő méretű mátrixokkal végeztem: 100, 300, 420, 696, 1000

Az összes algoritmuson 10x futtatam le, majd a 2 legkisebbet és legnagyobbat elhagytam, a többit pedig átlagoltam.

### Az eredmények
![](images/F1_diagram1.png)

![](images/F1_diagram2.png)
