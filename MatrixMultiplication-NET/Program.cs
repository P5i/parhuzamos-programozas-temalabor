﻿using System;
using System.Collections.Generic;
using System.Threading;

/*
    User hiba -> no exception
    Programmer hiba -> assertion
    Külső hiba -> exception
*/
namespace MatrixMultiplication_NET
{
    using MtxFun = Func<Matrix, Matrix, Matrix>;
    
    internal class Program
    {
        private static Matrix mtx1, mtx2;

        readonly static Dictionary<string, MtxFun> algorithms =
            new Dictionary<string, MtxFun>()
            {
                {"naive_column_major", Matrix.Multiply_naiveColumnMajor},
                {"naive_row_major", Matrix.Multiply_naiveRowMajor},
                {"thread", Matrix.Multiply_Thread},
                {"task", Matrix.Multiply_Task},
                {"simd", Matrix.Multiply_SIMD},
                {"task_simd", Matrix.Multiply_TaskSIMD}
            };
        
        public static int Main(string[] args)
        {
            CompareAlgorithms();
            return 0;
            
            if (args.Length != 2)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter a size and algorithm's name!");
                return 1;
            }
            
            if (!int.TryParse(args[0], out int size)) // Try-pattern
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter an integer for the size!");
                return 1;
            }

            mtx1 = Matrix.CreateRandom(size);
            Thread.Sleep(100);
            mtx2 = Matrix.CreateRandom(size);
        
            string algName = args[1];
            if (!algorithms.TryGetValue(algName, out var fun))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Please enter an existing algorithm's name!");
                return 1;
            }
            MeasureTime(fun, algName);
           
            return 0;
        }
        
        private static void MeasureTime(MtxFun fun, string alg)
        {
            using (new Timer(alg))
            {
                fun(mtx1, mtx2);
            }
        }

        private static void CompareAlgorithms()
        {
            int[] testSizes = {100, 300, 420, 696, 1000};

            foreach (int testSize in testSizes)
            {
                var testMtx1 = Matrix.CreateRandom(testSize);
                var testMtx2 = Matrix.CreateRandom(testSize);

                Console.WriteLine($"Test size: {testSize}");
                foreach (var alg in algorithms)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        using (new Timer($"{alg.Key}: {i}. timer", true))
                        {
                            alg.Value(testMtx1, testMtx2);
                        }
                    }
                    Console.WriteLine();
                }
                
            }
        }
        
    }
}