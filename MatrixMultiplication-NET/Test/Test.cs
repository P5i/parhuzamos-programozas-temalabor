using System;
using NUnit.Framework;

namespace MatrixMultiplication_NET.Test
{
    [TestFixture]
    public class NaiveColumnMajor
    {
        [TestCase]
        public void Size3()
        {
            Assert.AreEqual(Matrix.Multiply_naiveColumnMajor(Matricies.lhs_3, Matricies.rhs_3), Matricies.res_3);
        }
        
        [TestCase]
        public void Size69()
        {
            Assert.AreEqual(Matrix.Multiply_naiveColumnMajor(Matricies.lhs_69, Matricies.rhs_69), Matricies.res_69);
        }
        
    }

    [TestFixture]
    public class NaiveRowMajor
    {
        [TestCase]
        public void Size3()
        {
            Assert.AreEqual(Matrix.Multiply_naiveRowMajor(Matricies.lhs_3, Matricies.rhs_3), Matricies.res_3);
        }
        
        [TestCase]
        public void Size69()
        {
            Assert.AreEqual(Matrix.Multiply_naiveRowMajor(Matricies.lhs_69, Matricies.rhs_69), Matricies.res_69);
        }
    }
    
    [TestFixture]
    public class Thread
    {
        [TestCase]
        public void Size3()
        {
            Assert.AreEqual(Matrix.Multiply_Thread(Matricies.lhs_3, Matricies.rhs_3), Matricies.res_3);
        }
        
        [TestCase]
        public void Size69()
        {
            Assert.AreEqual(Matrix.Multiply_Thread(Matricies.lhs_69, Matricies.rhs_69), Matricies.res_69);
        }
    }
    
    [TestFixture]
    public class Task
    {
        [TestCase]
        public void Size3()
        {
            Assert.AreEqual(Matrix.Multiply_Task(Matricies.lhs_3, Matricies.rhs_3), Matricies.res_3);
        }
        
        [TestCase]
        public void Size69()
        {
            Assert.AreEqual(Matrix.Multiply_Task(Matricies.lhs_69, Matricies.rhs_69), Matricies.res_69);
        }
    }
    
    [TestFixture]
    public class SIMD
    {
        [TestCase]
        public void Size3()
        {
            //Assert.AreEqual(Matrix.Multiply_SIMD(Matricies.lhs_3, Matricies.rhs_3), Matricies.res_3);
        }
        
        [TestCase]
        public void Size4()
        {
            Assert.AreEqual(Matrix.Multiply_SIMD(Matricies.lhs_4, Matricies.rhs_4), Matricies.res_4);
        }
        
        [TestCase]
        public void Size6()
        {
            Assert.AreEqual(Matrix.Multiply_SIMD(Matricies.lhs_6, Matricies.rhs_6), Matricies.res_6);
        }
        
        [TestCase]
        public void Size16()
        {
            Assert.AreEqual(Matrix.Multiply_SIMD(Matricies.lhs_16, Matricies.rhs_16), Matricies.res_16);
        }
        
        [TestCase]
        public void Size69()
        {
            Assert.AreEqual(Matrix.Multiply_SIMD(Matricies.lhs_69, Matricies.rhs_69), Matricies.res_69);
        }
    }
}