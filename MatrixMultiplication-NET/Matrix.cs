using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MatrixMultiplication_NET
{   
    public struct Matrix : IEnumerable<int>
    {
        readonly int[] elements;
        readonly int size;
        private int elementCount; // only for the Add

        public Matrix(int size)
        {
            this.size = size;
            elements = new int[size * size];// alapból minden 0 lesz //PLAN stackalloc 
            elementCount = 0;
        }

        public void Add(int value)
        {
            Debug.Assert(elementCount <= size * size);
            elements[elementCount++] = value;
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<int> GetEnumerator() => elements.AsEnumerable().GetEnumerator();

        public static Matrix CreateRandom(int size)
        {
            var result = new Matrix(size);
            var random = new Random();           
            for (int i = 0; i < size * size; i++)
                result.elements[i] = random.Next(-1000, 1000);

            return result;
        }

        public int this[int row, int column]
        {
            get => elements[row * size + column];
            set => elements[row * size + column] = value;
        }


        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                    stringBuilder.Append($"{this[i, j]:D}, "); //this[].ToString("F3") format specifier
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public static bool operator==(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);

            int size = lhs.size;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (lhs[i, j] != rhs[i, j])
                        return false;
                }
            }

            return true;
        }

        public static bool operator!=(Matrix lhs, Matrix rhs)
            => !(lhs == rhs);
        

        public static Matrix operator*(Matrix lhs, Matrix rhs)
            => Multiply_TaskSIMD(lhs, rhs);

        public static Matrix Multiply_naiveColumnMajor(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;
            var result = new Matrix(size);
            
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    for (int k = 0; k < size; k++)
                        result[i, j] += lhs[i, k] * rhs[k, j];
            
            return result;
        }
        
        public static Matrix Multiply_naiveRowMajor(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;
            var result = new Matrix(size);
            
            for (int j = 0; j < size; j++)
                for (int i = 0; i < size; i++)
                    for (int k = 0; k < size; k++)
                        result[i, j] += lhs[i, k] * rhs[k, j];
        
            return result;
        }

        public static Matrix Multiply_Thread(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);

            int size = lhs.size;
            var result = new Matrix(size);

            /*int threadCnt = 2 * Environment.ProcessorCount;
            ThreadPool.SetMaxThreads(threadCnt, threadCnt);

            ThreadPool.GetAvailableThreads(out int workerThreads, out int completionPortThreads);
            Console.WriteLine($"{threadCnt} {workerThreads} {completionPortThreads} {Vector<int>.Count}");*/
            
            // Olyan mintha tudná a megfelelő szálszámot a pool-hoz
            
            using (var countdown = new CountdownEvent(size))
            {
                for (int i = 0; i < size; i++)
                {
                    int ii = i;
                    ThreadPool.QueueUserWorkItem(o =>
                    {
                        for (int j = 0; j < size; j++)
                            for (int k = 0; k < size; k++)
                                result[ii, j] += lhs[ii, k] * rhs[k, j];
                        
                        countdown.Signal();
                    });
                }

                countdown.Wait();
            }
            
            return result;
        }
        
        

        public static Matrix Multiply_Task(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;
            var result = new Matrix(size);

            Parallel.For(
                fromInclusive: 0,
                toExclusive: size,
                body: i =>
                {
                    for (int j = 0; j < size; j++)
                        for (int k = 0; k < size; k++)
                            result[i, j] += lhs[i, k] * rhs[k, j];
                }
            );
            
            return result;
        }

        private Matrix GetTranspose()
        {
            var res = new Matrix(this.size);
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    res[i, j] = this[j, i];
                
            return res;
        }
        
        public static Matrix Multiply_SIMD(Matrix lhs, Matrix rhs)
        {
            //Debug.Assert(Vector.IsHardwareAccelerated); // Támogatja a SIMD-t a CPU?
            Debug.Assert(lhs.size >= Vector<int>.Count);
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;
            var result = new Matrix(size);

            int simdLength = Vector<int>.Count;
            var rhst = rhs.GetTranspose();
            int until = size / simdLength;
            
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    int value = 0; // lokalitás kihasználása
                    int k = 0;
                    int i_size = i * size;
                    int j_size = j * size;
                    for (; k < until; k += simdLength)
                    {
                        var va = new Vector<int>(lhs.elements,  i_size + k);
                        var vb = new Vector<int>(rhst.elements, j_size + k);
                            
                        value += Vector.Dot(va, vb);
                    }

                    for (; k < size; k++)
                        value += lhs[i, k] * rhst[j, k];

                    result[i, j] = value;
                }
            }

            return result;
        }

        public static Matrix Multiply_TaskSIMD(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;
            var result = new Matrix(size);
            
            int simdLength = Vector<int>.Count;
            var rhst = rhs.GetTranspose();
            int until = size / simdLength;
            
            Parallel.For(
                fromInclusive: 0,
                toExclusive: size,
                body: i =>
                {
                    for (int j = 0; j < size; j++)
                    {
                        int value = 0; // lokalitás kihasználása
                        int k = 0;
                        int i_size = i * size;
                        int j_size = j * size;
                        for (; k < until; k += simdLength)
                        {
                            var va = new Vector<int>(lhs.elements,  i_size + k);
                            var vb = new Vector<int>(rhst.elements, j_size + k);
                            
                            value += Vector.Dot(va, vb);
                        }

                        for (; k < size; k++)
                            value += lhs[i, k] * rhst[j, k];

                        result[i, j] = value;
                    }
                }
            );
            
            return result;
        }

        // PLAN
        private static Matrix DivideAndConquer(Matrix lhs, Matrix rhs, Matrix result)
        {
            int size = lhs.size;
            if (size == 2)
            {
                result[0, 0] = lhs[0, 0] * rhs[0, 0] + lhs[0, 1] * rhs[1, 0];
                result[0, 1] = lhs[0, 0] * rhs[0, 1] + lhs[0, 1] * rhs[1, 1];
                result[1, 0] = lhs[1, 0] * rhs[0, 0] + lhs[1, 1] * rhs[1, 0];
                result[1, 1] = lhs[1, 0] * rhs[0, 1] + lhs[1, 1] * rhs[1, 1];
                return result;
            }
            else
            {
                int newSize = size / 2;
                
                var A_11 = new Matrix(newSize);
                for (int i = 0; i < newSize; i++)
                    for (int j = 0; j < newSize; j++)
                        A_11[i, j] = lhs[i, j];
                
                /*
                    Még madjnem ezt kéne 3x és 4x Bre
                 */
                
                //var C_11 = DivideAndConquer(A_11, B_11) + DivideAndConquer(A_12, B_21)
                //var C_12 = DivideAndConquer(A_11, B_12) + DivideAndConquer(A_12, B_22)
                //var C_21 = DivideAndConquer(A_21, B_11) + DivideAndConquer(A_22, B_21)
                //var C_22 = DivideAndConquer(A_21, B_12) + DivideAndConquer(A_12, B_22)
                
                // összerakom a mtx-t
                
                // rengeteg 'new'-van benne, várhatóan lassú lesz emiatt
            }
            return new Matrix(0);
        }
        
        //PLAN
        public static Matrix Multiply_DivideAndConquer(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;

            if (Math.Log(2, size) % 1 != 0)
            {
                int newSize = Convert.ToInt32(Math.Pow(2, Math.Ceiling(Math.Log(2, size))));
                
                var l = new Matrix(newSize);
                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        l[i, j] = lhs[i, j];
                
                var r = new Matrix(newSize);
                for (int i = 0; i < size; i++)
                    for (int j = 0; j < size; j++)
                        l[i, j] = rhs[i, j];

                lhs = l;
                rhs = r;
                size = newSize;
            }

            var result = new Matrix(size);
            DivideAndConquer(lhs, rhs, result);
            return result;
        }
        
        // PLAN
        public static Matrix Multiply_Strassen(Matrix lhs, Matrix rhs)
        {
            Debug.Assert(lhs.size == rhs.size);
            
            int size = lhs.size;
            var result = new Matrix(size);

            /*
                Ugyanolyan mint a Multiply_DivideAndConquer, csak a formulák mások -> 8 szorzás helyett 7 (de több +,-) így lesz gyorsabb
                de rengeteg 'new'-van benne, várhatóan lassú lesz emiatt
             */
            
            return result;
        }
    }
}